#pragma checksum "D:\ProjectWeb\Lab05_Dotnet\01_CitiesWebsite\CitiesWebsite\Views\Shared\_CityPopulation.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0ca3c32f25c051f6aef62cf0c6c03a54164ebf1a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__CityPopulation), @"mvc.1.0.view", @"/Views/Shared/_CityPopulation.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_CityPopulation.cshtml", typeof(AspNetCore.Views_Shared__CityPopulation))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0ca3c32f25c051f6aef62cf0c6c03a54164ebf1a", @"/Views/Shared/_CityPopulation.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b853c2693020103458b52d8246f923ca3fd89014", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__CityPopulation : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(61, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 4 "D:\ProjectWeb\Lab05_Dotnet\01_CitiesWebsite\CitiesWebsite\Views\Shared\_CityPopulation.cshtml"
   
    CitiesWebsite.Models.CityPopulation population = ViewBag.City.Population;

#line default
#line hidden
            BeginContext(152, 23, true);
            WriteLiteral("\r\n<h3> City Population ");
            EndContext();
            BeginContext(176, 15, false);
#line 8 "D:\ProjectWeb\Lab05_Dotnet\01_CitiesWebsite\CitiesWebsite\Views\Shared\_CityPopulation.cshtml"
                Write(population.Year);

#line default
#line hidden
            EndContext();
            BeginContext(191, 22, true);
            WriteLiteral("</h3>\r\n<p>\r\n    City: ");
            EndContext();
            BeginContext(214, 54, false);
#line 10 "D:\ProjectWeb\Lab05_Dotnet\01_CitiesWebsite\CitiesWebsite\Views\Shared\_CityPopulation.cshtml"
     Write(cityFormatter.GetFormattedPopulation(@population.City));

#line default
#line hidden
            EndContext();
            BeginContext(268, 24, true);
            WriteLiteral("\r\n</p>\r\n<p>\r\n    Urban: ");
            EndContext();
            BeginContext(293, 55, false);
#line 13 "D:\ProjectWeb\Lab05_Dotnet\01_CitiesWebsite\CitiesWebsite\Views\Shared\_CityPopulation.cshtml"
      Write(cityFormatter.GetFormattedPopulation(@population.Urban));

#line default
#line hidden
            EndContext();
            BeginContext(348, 24, true);
            WriteLiteral("\r\n</p>\r\n<p>\r\n    Metro: ");
            EndContext();
            BeginContext(373, 55, false);
#line 16 "D:\ProjectWeb\Lab05_Dotnet\01_CitiesWebsite\CitiesWebsite\Views\Shared\_CityPopulation.cshtml"
      Write(cityFormatter.GetFormattedPopulation(@population.Metro));

#line default
#line hidden
            EndContext();
            BeginContext(428, 6, true);
            WriteLiteral("\r\n</p>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public CitiesWebsite.Services.ICityFormatter cityFormatter { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
